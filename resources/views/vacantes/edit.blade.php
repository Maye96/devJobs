@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/medium-editor/5.23.3/css/medium-editor.min.css" integrity="sha256-R45gjjgTM82XinRpA4xKOL00zJ2/ajOSjY3tvw5JaDM=" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.0/dropzone.min.css" integrity="sha512-0ns35ZLjozd6e3fJtuze7XJCQXMWmb4kPRbb+H/hacbqu6XfIX0ZRGt6SrmNmv5btrBpbzfdISSd8BAsXJ4t1Q==" crossorigin="anonymous" />
@endsection

@section('navegacion')
   @include('ui.adminnav')
@endsection

@section('content')
    <h1 class="text-2xl text-center mt-10">Editar Vacante: {{$vacante->titulo}} </h1>

    <form action="{{ route('vacantes.update', ['vacante' => $vacante->id ]) }}" method = "POST" class="max-w-lg mx-auto my-10">
        @csrf
        @method('PUT')
        <!-- Titulo -->
        <div class="mb-5">
            <label for="titulo" class="block text-gray-700 text-sm mb-2">Titulo Vacante: </label>
            <input id="titulo" type="text" class="p-3 bg-gray-100 rounded form-input w-full @error('titulo') is-invalid @enderror" name="titulo"  autocomplete="new-password" placeholder="Titulo de la vacante" value="{{$vacante->titulo}}">
        
            @error('titulo')
                <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative mt-3 mb-6" role="alert">
                    <strong class="font-bold"> Error! </strong>
                    <span class="block">{{$message}}</span>
                </div>
            @enderror
        </div>
        
        <!-- Categoria -->
        <div class="mb-5">
            <label for="categoria" class="block text-gray-700 text-sm mb-2">Categoria: </label>

            <select id="categoria" class="block appearance-none w-full border border-gray-200 text-gray-700 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500 p-3 bg-gray-100 w-full" name="categoria">
                <option disabled selected>- Selecciona -</option>
                @foreach ($categorias as $categoria)
                    <option  {{ $vacante->categoria_id == $categoria->id ? 'selected' : '' }} value="{{ $categoria->id }}">
                        {{$categoria->nombre}}
                    </option>
                @endforeach
            </select>

            @error('categoria')
                <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative mt-3 mb-6" role="alert">
                    <strong class="font-bold">Error!</strong>
                    <span class="block"> {{$message}}</span>
                </div>
            @enderror
        </div>

        <!-- Experiencia -->
        <div class="mb-5">
            <label for="experiencia" class="block text-gray-700 text-sm mb-2" >Experiencia:</label>

            <select id="experiencia"  class="block appearance-none w-full  border border-gray-200 text-gray-700 rounded leading-tight  focus:outline-none focus:bg-white focus:border-gray-500 p-3 bg-gray-100 w-full" name="experiencia">
                <option disabled selected>-- Selecciona --</option>
                @foreach ($experiencias as $experiencia)
                    <option {{ $vacante->experiencia_id == $experiencia->id ? 'selected' : '' }} value="{{ $experiencia->id }}" >
                        {{$experiencia->nombre}}
                    </option>
                @endforeach
            </select>

            @error('experiencia')
                <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative mt-3 mb-6" role="alert">
                    <strong class="font-bold">Error!</strong>
                    <span class="block"> {{$message}}</span>
                </div>
            @enderror
        </div>

        <!-- Ubicacion -->
        <div class="mb-5">
            <label for="ubicacion"  class="block text-gray-700 text-sm mb-2">Ubicación:</label>

            <select
                id="ubicacion" class="block appearance-none w-full border border-gray-200 text-gray-700 rounded leading-tight  focus:outline-none focus:bg-white focus:border-gray-500 p-3 bg-gray-100 w-full" name="ubicacion" >
                <option disabled selected>- Selecciona -</option>
                @foreach ($ubicaciones as $ubicacion)
                    <option {{ $vacante->experiencia_id == $ubicacion->id ? 'selected' : '' }}  value="{{ $ubicacion->id }}">
                        {{$ubicacion->nombre}}
                    </option>
                @endforeach
            </select>

            @error('ubicacion')
                <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative mt-3 mb-6" role="alert">
                    <strong class="font-bold">Error!</strong>
                    <span class="block"> {{$message}}</span>
                </div>
            @enderror
        </div>


        <!-- Rango de Salario -->
        <div class="mb-5">
            <label  for="salario" class="block text-gray-700 text-sm mb-2">Salario:</label>

            <select
                id="salario"
                class="block appearance-none w-full  border border-gray-200 text-gray-700 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500 p-3 bg-gray-100" name="salario" >
                <option disabled selected>-- Selecciona --</option>
                @foreach ($salarios as $salario)
                    <option {{ $vacante->salario_id == $salario->id ? 'selected' : '' }}  value="{{ $salario->id }}">
                        {{$salario->nombre}}
                    </option>
                @endforeach
            </select>
            @error('salario')
                <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative mt-3 mb-6" role="alert">
                    <strong class="font-bold">Error!</strong>
                    <span class="block"> {{$message}}</span>
                </div>
            @enderror
        </div>

        <!-- Instanciar medium-editor - Descripcion -->
        <div class="mb-5">
            <label 
                for="descripcion" 
                class="block text-gray-700 text-sm mb-2"
            >Descripción del Puesto:</label>

            <div class="editable p-3 bg-gray-100 rounded form-input w-full text-gray-700"></div>
            <input type="hidden" name="descripcion" id="descripcion" value="{{$vacante->descripcion}}">
            @error('descripcion')
                <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative mt-3 mb-6" role="alert">
                    <strong class="font-bold">Error!</strong>
                    <span class="block"> {{$message}}</span>
                </div>
            @enderror
        </div>

        <!-- Instanciar medium-editor -->
        <div class="mb-5">
            <label 
                for="skills" 
                class="block text-gray-700 text-sm mb-2"
            >Habilidades y Conocimientos:
        <span class = "xs">Elige al menos 3</span> </label>
            <!-- Opciones de skills -->
            @php
                $skills = ['HTML5', 'CSS3', 'CSSGrid', 'Flexbox', 'JavaScript', 'jQuery', 'Node', 'Angular', 'VueJS', 'ReactJS', 'React Hooks', 'Redux', 'Apollo', 'GraphQL', 'TypeScript', 'PHP', 'Laravel', 'Symfony', 'Python', 'Django', 'ORM', 'Sequelize', 'Mongoose', 'SQL', 'MVC', 'SASS', 'WordPress', 'Express', 'Deno', 'React Native', 'Flutter', 'MobX', 'C#', 'Ruby on Rails']
            @endphp                                          <!-- Pasar al componente -->
            <lista-skills :skills="{{json_encode($skills)}}" :oldskills="{{json_encode($vacante->skills)}}"></lista-skills>

            @error('skills')
            <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative mt-3 mb-6" role="alert">
                <strong class="font-bold">Error!</strong>
                <span class="block"> {{$message}}</span>
            </div>
        @enderror
        </div>

        <!-- Imagen -->
        <div class="mb-5">
            <label 
                for="imagen" 
                class="block text-gray-700 text-sm mb-2"
            >Imagen Vacante:</label>

            <div id="dropzoneDevJobs" class="dropzone rounded bg-gray-100"></div>
            <input type="hidden" name="imagen" id="imagen" value="{{$vacante->imagen}}">
            @error('imagen')
                <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative mt-3 mb-6" role="alert">
                    <strong class="font-bold">Error!</strong>
                    <span class="block"> {{$message}}</span>
                </div>
            @enderror
            <p id= "error"></p>
        </div>

      
        

        <button type="submit" class="bg-teal-500 w-full hover:bg-teal-600 text-gray-100 font-bold p-3 focus:outline focus:shadow-outline uppercase"> Publicar Vacante</button>
    </form>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/medium-editor/5.23.3/js/medium-editor.min.js" integrity="sha256-R0a97wz9RimQA9BJEMqcwuOckEMhIQcdtij32P5WpuI=" crossorigin="anonymous"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.0/min/dropzone.min.js" integrity="sha512-Mn7ASMLjh+iTYruSWoq2nhoLJ/xcaCbCzFs0ZrltJn7ksDBx+e7r5TS7Ce5WH02jDr0w5CmGgklFoP9pejfCNA==" crossorigin="anonymous"></script>
<script>
    Dropzone.autoDiscover = false;
    document.addEventListener('DOMContentLoaded', () => {
        // Medium Editor
        const editor = new MediumEditor('.editable',{
            // Objetos de Configuacion (negrita, subrayado, justificacion ...)
            toolbar: {
                buttons: ['bold', 'italic', 'underline', 'quote', 'anchor', 'justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull', 'orderedlist', 'unorderedlist','h2', 'h3'],
                // Para que la barra se mantenga quieta y centrada
                static:true,
                sticky:true
            },
            placeholder: {
                text: 'Información de la vacante'
            }
        });
        // Para que tome todo lo que se vaya escribiendo en el medium-editor (Contenido que en tiempo real escribe el usuario)
        editor.subscribe('editableInput', function(eventObj, editable){
            // getContent = Trae el contenido que tenga el editor
            const contenido = editor.getContent();
            document.querySelector('#descripcion').value = contenido;
        })

        // Llena el editor con el contenido del input hidden
        editor.setContent(document.querySelector('#descripcion').value);
        // Dropzone
        // Instanciarlo
        const dropzoneDevJobs = new Dropzone('#dropzoneDevJobs',{
            url: "/vacantes/imagen",
            // Cambiar el texto del mensaje
            dicDefaultMessage: 'Sube Aqui tu archivo',
            // Validar que solo se suban archivos
            acceptedFiles:".png, .jpg, .jpeg, .gif, .bmp",
            // Agregar para eliminar archivo en dropzone
            addRemoveLinks: true,
            // Cambiar el nombre de remove file
            dictRemoveFile: 'Borrar Achivo',
            // Indicar la maxima cantidad de archivos
            maxFiles: 1,
            // Para enviar el dropzone con el csrf, Laravel necesita el token para el post
            headers: {
                'X-CSRF-TOKEN': document.querySelector('meta[name=csrf-token]').content
            },
            // Cuando falle validacion que se ejecute:
            init: function() {
                // Revisar si posee imagen
                if(document.querySelector('#imagen').value.trim()){
                    // console.log('Si hay imagen')

                    /// Se debe recrear el objeto del dropzone, para mostrar en el input dentro del old()
                    // Crear un objeto
                    let imagenPublicada = {};
                    imagenPublicada.size = 1234;
                    imagenPublicada.name= document.querySelector('#imagen').value;
                    imagenPublicada.nombreServidor = document.querySelector('#imagen').value
                    // Crear un objeto y agregar a la parte de dropzone
                    this.options.addedfile.call(this, imagenPublicada);
                    // Traer la imagen del dropzone, qu se lee en input hidden
                    this.options.thumbnail.call(this, imagenPublicada, `/storage/vacantes/${imagenPublicada.name}`);


                    imagenPublicada.previewElement.classList.add('dz-sucess');
                    imagenPublicada.previewElement.classList.add('dz-complete');

                }
            },
            success: function(file, response) {
                // console.log(file);
                // console.log(response);
                console.log(response.correcto);
                document.querySelector('#error').textContent = '';

                // Coloca la respuesta del servidor en el input hidden
                document.querySelector('#imagen').value = response.correcto;

                // Anhadir al objeto de archivo el nombre del servidor
                file.nombreServidor = response.correcto;
            },
            maxfilesexceeded: function(file) {
                console.log('muchos archivo');
                // Para acceder a los archivos subidos, usar: this.files
                if(this.files[1] != null) {
                    this.removeFile(this.files[0]); // Elimina el archivo anterior
                    this.addFile(file); // Agrega el nuevo archivo (e; que esta demas)
                }
            },
            // Saber cual es la imagen que el usuario elimino
            removedfile: function(file, response) {
                // Eliminar imagen reemplazada en el DOM
                file.previewElement.parentNode.removeChild(file.previewElement);
                params = {
                    // Revise la imagen con el nombre del servidor o => '??' la del dom
                    imagen:file.nombreServidor
                }
                axios.post('/vacantes/borrarimagen', params)
                    .then(respuesta => console.log(respuesta))
            }
        });
    });
</script>
@endsection
