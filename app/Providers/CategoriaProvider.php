<?php

namespace App\Providers;

use App\Categoria;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class CategoriaProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Indicar que se debe pasar a todas las vistas
        View::composer('*', function($view){
            $categorias = Categoria::all();
            $view->with('categorias', $categorias);
        });
    }
}
