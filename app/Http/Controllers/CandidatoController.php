<?php

namespace App\Http\Controllers;

use App\Vacante;
use App\Candidato;
use Illuminate\Http\Request;
use App\Notifications\NuevoCandidato;

class CandidatoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Para ver lo que se envia en la ruta. Para obtener el ID actual
        // dd($request->route('id'));

        $id_vacante = $request->route('id');

        // Obtener los candidatos y la vacante
        $vacante = Vacante::findOrFail($id_vacante);

        $this->authorize('view', $vacante);
        
        return view('candidatos.index', compact('vacante'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validacion
        $data = $request->validate([
            'nombre' => 'required',
            'email' => 'required|email',
            'cv' => 'required|min:pdf|max:1000',
            'vacante_id' => 'required',
        ]);

        // Almacenar archivo pdf
        // Verificar que hay un archivo
        if($request->file('cv'))
        {
            $archivo = $request->file('cv');
            $nombreArchivo = time(). ".".$request->file('cv')->extension();
            $ubicacion = public_path('/storage/cv');
            $archivo->move($ubicacion, $nombreArchivo);
        }
        // Almacenar en la DB

        /*** Cuarta forma: Usando relaciones ***/
        // El controlador o tiene como parametro vacante , entonces
        $vacante = Vacante::find($data['vacante_id']);
        $vacante->candidatos()->create([
            'nombre' => $data['nombre'],
            'email' => $data['email'],
            'cv' => $nombreArchivo,
        ]);

        // Una vez se cree un candidato se enviara una notificacion al reclutador
        $reclutador = $vacante->reclutador;
        $reclutador->notify( new NuevoCandidato($vacante->titulo,$vacante->id));

        /*** Una forma 
        // Crea una instancia de candidato
        $candidato = new Candidato();
        $candidato->nombre = $data['nombre'];
        $candidato->email = $data['email'];
        $candidato->vacante_id = $data['vacante_id'];
        $candidato->cv = 'prueba.pdf';
        ***/

        /*** Segunda Forma 
        $candidato = new Candidato($data);
        $candidato->cv = 'prueba.pdf';
        $candidato->save();
        ***/

        /*** Tercera Forma 
        $candidato = new Candidato();
        $candidato->fill($data);
        $candidato->cv = 'prueba.pdf';
        $candidato->save();
        ***/

        // Redirect que lleva a la pagina previa
        return back()->with('estado', 'Tus datos se enviaron correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Candidato  $candidato
     * @return \Illuminate\Http\Response
     */
    public function show(Candidato $candidato)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Candidato  $candidato
     * @return \Illuminate\Http\Response
     */
    public function edit(Candidato $candidato)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Candidato  $candidato
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Candidato $candidato)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Candidato  $candidato
     * @return \Illuminate\Http\Response
     */
    public function destroy(Candidato $candidato)
    {
        //
    }
}
