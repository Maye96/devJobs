<?php

namespace App\Http\Controllers;

use App\Salario;
use App\Vacante;
use App\Categoria;
use App\Ubicacion;
use App\Experiencia;
use Facade\FlareClient\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class VacanteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // Ver las vacantes del usuario autentificado
        // $vacantes = auth()->user()->vacantes;
        $vacantes = Vacante::where('user_id', auth()->user()->id)->latest()->simplePaginate(10);
        //ya qe paginate tiene clases de bootstrap y se usa twidcss reemplazar simplePaginate
        return view('vacantes.index', compact('vacantes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Consultas
        $categorias = Categoria::all();
        $experiencias = Experiencia::all();
        $ubicaciones = Ubicacion::all();
        $salarios = Salario::All();

        return view('vacantes.create', compact('categorias', 'experiencias', 'ubicaciones', 'salarios'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validacion
        $data = $request->validate([
            'titulo' => 'required|min:8',
            'categoria' => 'required',
            'experiencia' => 'required',
            'ubicacion' => 'required',
            'salario' => 'required',
            'descripcion' => 'required|min:50',
            'imagen' => 'required',
            'skills' => 'required',
        ]);
       

        // Almacenar en la DB 
        auth()->user()->vacantes()->create([
            'titulo' => $data['titulo'],
            'imagen' => $data['imagen'],
            'descripcion' => $data['descripcion'],
            'skills' => $data['skills'],
            'categoria_id' => $data['categoria'],
            'experiencia_id' => $data['experiencia'],
            'ubicacion_id' => $data['ubicacion'],
            'salario_id' => $data['salario']
        ]);

        return redirect()->action('VacanteController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Vacante  $vacante
     * @return \Illuminate\Http\Response
     */
    public function show(Vacante $vacante)
    {
        //
        // if($vacante->actia == 0) return abort(404);
        
        return view('vacantes.show')->with('vacante', $vacante);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Vacante  $vacante
     * @return \Illuminate\Http\Response
     */
    public function edit(Vacante $vacante)
    {
        // Policy
                        /* Metodo, Vacante actual */
        $this->authorize('view', $vacante);
        // Consultas
        $categorias = Categoria::all();
        $experiencias = Experiencia::all();
        $ubicaciones = Ubicacion::all();
        $salarios = Salario::All();

        return view('vacantes.edit', compact('vacante','categorias', 'experiencias', 'ubicaciones', 'salarios'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vacante  $vacante
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vacante $vacante)
    {
        // dd($request->all());

        // Policy
        $this->authorize('update', $vacante);

        // Validacion
        $data = $request->validate([
            'titulo' => 'required|min:8',
            'categoria' => 'required',
            'experiencia' => 'required',
            'ubicacion' => 'required',
            'salario' => 'required',
            'descripcion' => 'required|min:50',
            'imagen' => 'required',
            'skills' => 'required',
        ]);

        $vacante->titulo = $data['titulo'];
        $vacante->skills = $data['skills'];
        $vacante->imagen = $data['imagen'];
        $vacante->descripcion = $data['descripcion'];
        $vacante->categoria_id = $data['categoria'];
        $vacante->experiencia_id = $data['experiencia'];
        $vacante->ubicacion_id = $data['ubicacion'];
        $vacante->salario_id = $data['salario'];

        $vacante->save();

        // Redireccionar
        return redirect()->action('VacanteController@index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Vacante  $vacante
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Vacante $vacante)
    {
        // Policy
        $this->authorize('delete', $vacante);

        // return response()->json($vacante);
        $vacante->candidatos()->delete();
        $vacante->delete();

        return response()->json(['mensaje' => 'se elimino la vacante' . $vacante->titulo ]);
    }

    public function imagen(Request $request)
    {
        $imagen = $request->file('file');
        // definir el nombre unico. time() asigna el nombre segun la hora que se subio al servidor
        $nombreImagen = time().'.'.$imagen->extension();
        // Mueve la imagen a la carpeta vacante
        $imagen->move(public_path('storage/vacantes'), $nombreImagen);
        return response()->json(['correcto'=> $nombreImagen]);
    }
    
    // Borrar imagen via Ajax
    public function borrarimagen(Request $request)
    {
        if($request->ajax())
        {
            $imagen = $request->get('imagen');
            // Verificar que la imagen existe
            if(File::exists('storage/vacantes/' . $imagen))
            {
                File::delete('storage/vacantes/' . $imagen);
            }
            return response('Imagen Eliminada', 200);
        }
    }

    // Cambia el estado de una vacante
    public function estado(Request $request, Vacante $vacante)
    {
        // Leer nuevo estado y asignarlo
        $vacante->activa = $request->estado;

        // Guardarlo en la DB
        $vacante->save();

        return response()->json(['respuesta' => 'Correcto']);

    }

    // Buscar
    public function buscar(Request $request)
    {
        // dd($request->all());
        
        // Validar campos de busqueda
        $data = $request->validate([
            'categoria' => 'required',
            'ubicacion' => 'required'
        ]);

        // Asignar valores
        $categoria = $data['categoria'];
        $ubicacion = $data['ubicacion'];

        
        $vacantes = Vacante::latest()
            ->where('categoria_id', $categoria)
            ->Where('ubicacion_id', $ubicacion)
            ->get();
        
        
        /*****
        $vacantes = Vacante::where([
            'categoria_id' => $categoria,
            'ubicacion_id' => $ubicacion 
        ])->get();
        *****/

        return view('buscar.index', compact('vacantes'));
    }

    // Resultados
    public function resultados()
    {
        return "Mostrando Resultado";
    }
}
