<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vacante extends Model
{
    //
    protected $fillable = [
        'titulo',
        'imagen',
        'descripcion',
        'skills',
        'activa',
        'categoria_id',
        'experiencia_id',
        'ubicacion_id',
        'salario_id',
        'user_id',
    ];

    // Relacion 1:1 Una vacante tiene una categoria
    public function categoria() 
    {
        return $this->belongsTo(Categoria::class);
    }

    // Relacion 1:1 Una vacante tiene un salario
    public function salario() 
    {
        return $this->belongsTo(Salario::class);
    }

    // Relacion 1:1 Una vacante tiene una ubicacion
    public function ubicacion() 
    {
        return $this->belongsTo(Ubicacion::class);
    }

    // Relacion 1:1 Una vacante tiene una ubicacion
    public function experiencia() 
    {
        return $this->belongsTo(Experiencia::class);
    }

    // Relacion 1:1 recultador publica una vacante
    public function reclutador()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    // Relacion 1:n Una vacante va a terner muchos candidatos
    public function candidatos()
    {
        return $this->hasMany(Candidato::class);
    }
}
